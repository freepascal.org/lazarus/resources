The svg files contained in this folder were kindly provided by 
Roland Hahn (https://www.rhsoft.de/).

License:
Creative Commons CC0 1.0 Universal
(freely available, no restrictions in usage)